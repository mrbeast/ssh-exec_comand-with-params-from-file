import os
import subprocess
import re
import time
import sys
import paramiko

content = []

with open(r"path\to\file\test.txt") as f:
    content = f.readlines()

ip='some_ip'
port=22
username='login'
password='password'

ssh=paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(ip,port,username,password)

for strv in content:
 v = strv.split(' ')
 stdin,stdout,stderr=ssh.exec_command('curl -sm60 http://' + v[1] + ':2032/' + v[0] + '/info | jq .')
 outlines=stdout.readlines()
 resp=''.join(outlines)
 print(resp)


